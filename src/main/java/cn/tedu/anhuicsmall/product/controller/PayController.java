package cn.tedu.anhuicsmall.product.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 支付宝的支付控制类
 *
 * @Author java@Wqy
 * @Version 0.0.1
 */
@Api(tags = "12.支付模块")
@Slf4j
@Validated
@RestController
@RequestMapping("/pay")
public class PayController {

    public PayController(){
        log.debug("创建支付控制器类：PayController");
    }

    // APPID
    @Value("${PAY.APP_ID}")
    private String APP_ID;
    // 支付宝私钥
    @Value("${PAY.APP_PRIVATE_KET}")
    private String APP_PRIVATE_KET;
    // 支付宝公钥
    @Value("${PAY.ALIPAY_PUBLIC_KEY}")
    private String ALIPAY_PUBLIC_KEY;
    // 支付宝网关地址
    @Value("${PAY.GATEWAY_URL}")
    private String GATEWAY_URL;
    // 支付宝同步通知路径，也就是当付款完毕后跳转本项目的页面，可私网地址
    @Value("${PAY.RETURN_URL}")
    private String RETURN_URL;
    // 字符集
    @Value("${PAY.CHARSET}")
    private static final String CHARSET = "UTF-8";
    // 序列化方式
    @Value("${PAY.FORMAT}")
    private static final String FORMAT = "JSON";
    // 签名方式
    @Value("${PAY.SIGN_TYPE}")
    private static final String SIGN_TYPE = "RSA2";
    // 支付宝一步通知路径，公网地址
    private static final String NOTIFY_URL = "";

    /**
     * 处理请求支付的方法
     * @param session 建立HTTP回话
     * @param model 组件
     * @param donaMoney 商品价格
     * @param donaId 商品id
     * @param subject 商品名称
     * @return 返回请求成功后的商品信息
     * @throws AlipayApiException 支付API的内置异常处理
     */
    @GetMapping("/alipay")
    public String alipay(HttpSession session, Model model, @RequestParam(value = "dona_money") float donaMoney,
                         @RequestParam(value = "dona_id") int donaId,
                         @RequestParam(value = "dona_name") String subject) throws AlipayApiException {
        // 把项目id放在session中
        session.setAttribute("dona_id",donaId);

        // 生成订单号（支付宝的要求）
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String user = UUID.randomUUID().toString().replace("-", "").toUpperCase();

        String orderNum = time+user;
        System.out.println("订单号："+orderNum);

        // 调用封装好的方法（给支付宝接口发送请求）
        return sendRequestToAlipay(orderNum,donaMoney,subject);
    }

    /**
     * 请求成功后返回商品结果
     * @param outTradeNo 订单号
     * @param totalAmount 商品价格
     * @param subject 商品名称
     * @return 返回结果
     * @throws AlipayApiException 支付API的内置异常处理
     */
    private String sendRequestToAlipay(String outTradeNo,Float totalAmount,String subject) throws AlipayApiException {
        // 获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL,APP_ID,APP_PRIVATE_KET,FORMAT,CHARSET,ALIPAY_PUBLIC_KEY,SIGN_TYPE);

        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setNotifyUrl(NOTIFY_URL); // 支付宝通知的本地地址
        alipayRequest.setReturnUrl(RETURN_URL); // 支付宝异步回调的公网地址

        // 商品描述
        String body = "";
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + outTradeNo + "\","
                        + "\"total_amount\":\"" + totalAmount + "\","
                        + "\"subject\":\"" + subject + "\","
                        + "\"body\":\"" + body + "\","
                        + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        // 请求
        String result =  alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println("商品信息结果："+result);
        return result;
    }

    /**
     * 该方法可以获取支付成功后,支付宝回调时传递的信息
     * @param request HttpServletRequest
     * @param session 回话
     * @param model model
     * @return 返回页面信息
     * @throws UnsupportedEncodingException 不支持的转码异常
     * @throws AlipayApiException  支付时发生的异常
     */
    @RequestMapping("/returnUrl")
    public String returnUrlMethod(HttpServletRequest request,HttpSession session,Model model) throws UnsupportedEncodingException, AlipayApiException {
        log.debug("执行同步回调...");
        // 获取支付宝GET请求过来的反馈信息
        Map<String,String> params = new HashMap<>();
        Map<String,String[]> requestParams = request.getParameterMap(); // 获取get参数列表
        for (String name : requestParams.keySet()) {
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 解决乱码
            valueStr = new String(valueStr.getBytes("ISO_8859_1"), "utf-8");
            params.put(name, valueStr);
        }

        log.debug("支付宝传递过来的参数：{}",params);
        // 验证签名(支付宝公钥)
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, CHARSET, SIGN_TYPE);
        //验证签名通过
        if(signVerified){
            // 商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO_8859_1"), "UTF-8");

            // 支付宝交易流水号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO_8859_1"), "UTF-8");

            // 付款金额
            float money = Float.parseFloat(new String(request.getParameter("total_amount").getBytes("ISO_8859_1"), "UTF-8"));

            System.out.println("商户订单号="+out_trade_no);
            System.out.println("支付宝交易号="+trade_no);
            System.out.println("付款金额="+money);

            //在这里编写自己的业务代码（对数据库的操作）
			/*
			################################
			*/
            //跳转到提示页面（成功或者失败的提示页面）
            model.addAttribute("flag",1);
            model.addAttribute("msg","支持");
            return "common/payTips";
        }else{
            //跳转到提示页面（成功或者失败的提示页面）
            model.addAttribute("flag",0);
            model.addAttribute("msg","支持");
            return "common/payTips";
        }
    }

}
